# 💻 Sobre

### Projeto construído utilizando **React.js** com **create-react-app**

# 🖥 Itens

- Login de usuários
- Listagem de empresas
- Detalhamento de empresas
- Documentação e exemplo interativo de componentes ( Storybook )
- Responsividade

# 💡 Implementações futuras

- Documentação das páginas no storybook
- Trabalhar com ações no storybook, para botões, onBlur, submit e etc
- Paginação com "infinite scroll" na /home ao mostrar os cards
- Logout
- Integração contínua com Travis
- 100% de cobertura de testes

# 📦 Dependências

- **AXIOS** para requisições
- **PROP-TYPES** para documentar e tipar as propriedades de componentes
- **STYLED-COMPONENTS** para poder usar JS dentro do CSS e auxiliar na estilização

# 🚀 Rodando o projeto

1. Clone o projeto para sua máquina

```
git clone https://bernardomafra74@bitbucket.org/bernardomafra74/empresas-web.git
```

2. Instale as dependências com **yarn** ou **npm**

```bash
    yarn
    # ou
    npm install
```

3. Iniciar o projeto no navegador

```bash
    yarn start
    # ou
    npm run start
```

4. Iniciar o storybook

```bash
    yarn storybook
    # ou
    npm run storybook
```

5. Rodar os testes

```bash
    yarn test
    # ou
    npm run test
```
