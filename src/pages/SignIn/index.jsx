import React from "react";
import Password from "../../assets/ic-cadeado.svg";
import Mail from "../../assets/ic-email.svg";
import IoasysLogo from "../../assets/logo-home.png";
import Button from "../../components/Button";
import Input from "../../components/Input";
import Loading from "../../components/Loading";
import useForm from "../../hooks/useForm";
import makeAuthService from "../../infra/factories/auth";
import history from "../../routes/history";
import { setCookies } from "../../utils";
import * as Styles from "./styles";

const SignIn = () => {
  const email = useForm("mail");
  const password = useForm("password");

  const [isLoading, setIsLoading] = React.useState(false);

  async function handleSignIn(e) {
    e.preventDefault();

    const emailValid = email.validate();
    const passwordValid = password.validate();

    if (emailValid && passwordValid) {
      const api = makeAuthService();
      setIsLoading(true);
      const response = await api.signIn(email.value, password.value);
      setIsLoading(false);

      if (response.body.success) {
        setIsLoading(false);
        const { client, uid } = response.headers;
        setCookies("access-token", response.headers["access-token"], {
          expire_in: response.headers.expiry,
        });
        setCookies("client", client);
        setCookies("uid", uid);
        history.push("/home");
      } else {
        const error = response.body.errors[0];
        email.setError(error);
        password.setError(error);

        email.setValue("");
        password.setValue("");
        return;
      }
    }
  }

  return (
    <Styles.Container>
      <img src={IoasysLogo} alt="ioasys-logo-home" />
      <h4>BEM-VINDO AO EMPRESAS</h4>
      <p>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </p>
      <form onSubmit={handleSignIn}>
        <Input
          label="E-mail"
          name="mail"
          type="text"
          icon={Mail}
          placeholder="E-mail"
          onChange={email.onChange}
          onBlur={email.onBlur}
          value={email.value}
          error={email.error}
        />
        <Input
          label="Senha"
          name="pass"
          type="password"
          icon={Password}
          placeholder="Senha"
          onChange={password.onChange}
          onBlur={password.onBlur}
          value={password.value}
          error={password.error}
        />
        <Button type="submit">ENTRAR</Button>
      </form>
      {isLoading && <Loading />}
    </Styles.Container>
  );
};

export default SignIn;
