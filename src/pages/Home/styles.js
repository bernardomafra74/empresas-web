import styled, { css } from "styled-components";

export const Container = styled.div`
  background-color: ${({ theme: { colors } }) => colors.homeBg};
  height: -webkit-fill-available;
  width: -webkit-fill-available;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 5.638rem;

  h4 {
    margin: 1rem;
    font-family: ${({ theme: { fonts } }) => fonts.textStyle5.family};
    font-size: ${({ theme: { fonts } }) => fonts.textStyle5.size};
    font-weight: ${({ theme: { fonts } }) => fonts.textStyle5.weight};
    color: ${({ theme: { colors } }) => colors.blue};
  }

  nav {
    z-index: 1000;

    width: 100vw;
    display: flex;
    align-items: center;
    justify-content: center;
    position: fixed;
    transition: 0.5s;
    top: 0px;

    min-height: 5.438rem;

    background-image: ${({ theme: { colors } }) =>
      `linear-gradient(180deg, ${colors.mediumPink} 44%, ${colors.nightBlue} 206%)`};

    /* background-image: ${({ theme: { colors } }) =>
      `linear-gradient(173deg, ${colors.mediumPink} 24%, ${colors.nightBlue} 66%)`}; */

    img {
      width: 120px;
      height: 32px;
    }

    svg {
      position: absolute;
      right: 2.5rem;
      width: 40px;
      height: 40px;

      cursor: pointer;
    }

    ${({ expandSearch, theme: { colors } }) =>
      expandSearch &&
      css`
        transition: position 1s ease;
        div.input-container {
          margin-top: 1rem;
          padding-bottom: 0;
          input {
            padding-left: 3rem;
            width: 95vw;
            color: ${colors.whiteTwo};
            margin-bottom: 0.5rem;
          }
          span.line {
            width: 95vw;
            background-color: ${colors.whiteTwo};
          }
        }

        img {
          display: none;
        }
        svg {
          right: 0;
          left: 2.5rem;
        }
      `}
  }

  @media (max-width: 600px) {
    nav {
      svg {
        left: 0.5rem;
      }
    }
  }
`;
