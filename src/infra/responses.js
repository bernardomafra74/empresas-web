import { DontContentError, InvalidParamError, ServerError } from "./errors";

const HashMapResponse = new Map();

HashMapResponse.set(500, (data) => {
  return new ServerError(data);
});
HashMapResponse.set(400, (data) => {
  return new InvalidParamError(data);
});
HashMapResponse.set(401, (data) => {
  return data;
});
HashMapResponse.set(204, (data) => {
  return new DontContentError(data);
});
HashMapResponse.set(200, (_) => _);
HashMapResponse.set(201, (_) => _);

export default HashMapResponse;
