import { getCookie } from "../../utils";
import { MissingParamError } from "../errors";

export class EnterpriseService {
  constructor(httpClient, hashMapHttpResponse, baseUrl) {
    this.httpClient = httpClient;
    this.hashMapHttpResponse = hashMapHttpResponse;
    this.baseUrl = baseUrl;
  }

  static getAccessTokens() {
    const accessToken = getCookie("access-token");
    if (!accessToken) {
      return new MissingParamError("accessToken");
    }

    const client = getCookie("client");
    if (!client) {
      return new MissingParamError("client");
    }

    const uid = getCookie("uid");
    if (!uid) {
      return new MissingParamError("uid");
    }

    return {
      accessToken,
      client,
      uid,
    };
  }

  async getAll() {
    const { accessToken, client, uid } = EnterpriseService.getAccessTokens();

    const response = await this.httpClient.request({
      url: `${this.baseUrl}/enterprises`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "access-token": accessToken,
        client: client,
        uid: uid,
      },
    });
    return this.hashMapHttpResponse.get(response.statusCode)(response);
  }

  async getOne(name) {
    if (!name) {
      return new MissingParamError("name");
    }

    const { accessToken, client, uid } = EnterpriseService.getAccessTokens();

    const response = await this.httpClient.request({
      url: `${this.baseUrl}/enterprises?name=${name}`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "access-token": accessToken,
        client: client,
        uid: uid,
      },
    });
    return this.hashMapHttpResponse.get(response.statusCode)(response);
  }
}
