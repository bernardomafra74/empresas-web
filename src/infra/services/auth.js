export class AuthService {
  constructor(httpClient, hashMapHttpResponse, baseUrl) {
    this.httpClient = httpClient;
    this.hashMapHttpResponse = hashMapHttpResponse;
    this.baseUrl = baseUrl;
  }

  async signIn(email, password) {
    const request = {
      email,
      password,
    };

    const response = await this.httpClient.request({
      url: `${this.baseUrl}/users/auth/sign_in`,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      body: request,
    });
    return this.hashMapHttpResponse.get(response.statusCode)(response);
  }
}
