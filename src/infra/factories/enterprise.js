import HttpClient from "../adapter";
import HttpHashMapResponses from "../responses";
import { EnterpriseService } from "../services/enterprise";

const makeEnterpriseService = () => {
  return new EnterpriseService(
    HttpClient,
    HttpHashMapResponses,
    process.env.REACT_APP_ENTERPRISE_API_URL
  );
};
export default makeEnterpriseService;
