import HttpClient from "../adapter";
import HttpHashMapResponses from "../responses";
import { AuthService } from "../services/auth";

const makeAuthService = () => {
  return new AuthService(
    HttpClient,
    HttpHashMapResponses,
    process.env.REACT_APP_ENTERPRISE_API_URL
  );
};
export default makeAuthService;
