import PropTypes from "prop-types";
import React from "react";
import { Redirect, Route } from "react-router-dom";
import { getCookie } from "../utils";

export default function RouteWrapper({ isPrivate, ...rest }) {
  const signed = getCookie("access-token");

  if (!signed && isPrivate) {
    return <Redirect to="/" />;
  }

  if (signed && !isPrivate) {
    return <Redirect to="/home" />;
  }

  return <Route {...rest} />;
}

RouteWrapper.propTypes = {
  isPrivate: PropTypes.bool,
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
    .isRequired,
};

RouteWrapper.defaultProps = {
  isPrivate: false,
};
