import React from "react";
import { Router, Switch } from "react-router-dom";
import history from "./history";
import routesSchema from "./routes";
import RouteWrapper from "./RouteWrapper";

export default function Routes() {
  return (
    <Router history={history}>
      <Switch>
        {routesSchema.map((route) => (
          <RouteWrapper
            key={route.key}
            path={route.path}
            exact={route.exact}
            component={route.component}
            isPrivate={route.isPrivate}
          />
        ))}
      </Switch>
    </Router>
  );
}
