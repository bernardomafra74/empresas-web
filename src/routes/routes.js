import Home from "../pages/Home";
import SignIn from "../pages/SignIn";

const routesSchema = [
  {
    key: "route-sign-in-0",
    component: SignIn,
    path: "/",
    exact: true,
    isPrivate: false,
  },
  {
    key: "route-home-1",
    component: Home,
    path: "/home",
    exact: true,
    isPrivate: true,
  },
];

export default routesSchema;
