import styled, { css } from "styled-components";

export const Card = styled.div`
  transition: all 1s ease;

  overflow-x: hidden;

  display: grid;
  grid-template-columns: 1fr 3fr;
  grid-template-rows: 1fr 1fr;
  grid-template-areas:
    "image smallText more"
    "descriptionBegin d descriptionEnd";
  width: 57.69rem;
  height: 13.366rem;
  padding: 1.688rem 1rem 1.679rem 1.914rem;
  margin: 0.3rem 0px;
  background-color: ${({ theme: { colors } }) => colors.whiteTwo};
  border-radius: 0.294rem;

  img {
    grid-area: image;
    width: 18.313rem;
    height: 10rem;
    margin-right: 2.397rem;

    ${({ hasPhoto, theme: { colors } }) =>
      !hasPhoto &&
      css`
        background-color: ${colors.warmGrey};
        color: ${colors.darkIndigo};
        font-size: 1.5rem;
        text-align: center;
      `};

    object-fit: contain;
  }

  ${({ showInfo }) =>
    showInfo &&
    css`
      height: auto;
      max-height: 25rem;
      min-height: 13.366rem;
      padding-right: 1rem;
    `}

  @media (max-width: 600px) {
    grid-template-columns: 1fr;
    grid-template-rows: 2fr 1fr 1fr 4fr;
    grid-template-areas:
      "image"
      "smallText"
      "more"
      "description";

    width: -webkit-fill-available;
    flex-direction: column;
    height: 19.366rem;

    padding: 1rem;

    img {
      width: 90vw;
      margin-right: 0;
      object-fit: cover;
    }

    ${({ showInfo }) =>
      showInfo &&
      css`
        height: auto;
      `}
  }
`;

export const CardBody = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;

  grid-area: smallText;

  span {
    font-family: ${({ theme: { fonts } }) => fonts.textStyle6.family};
    font-size: ${({ theme: { fonts } }) => fonts.textStyle6.size};
    font-weight: ${({ theme: { fonts } }) => fonts.textStyle6.weight};
    color: ${({ theme: { colors } }) => colors.darkIndigo};
  }

  h5 {
    font-family: ${({ theme: { fonts } }) => fonts.textStyle7.family};
    font-size: ${({ theme: { fonts } }) => fonts.textStyle7.size};
    font-weight: ${({ theme: { fonts } }) => fonts.textStyle7.weight};
    color: ${({ theme: { colors } }) => colors.warmGrey};
  }

  p {
    font-family: ${({ theme: { fonts } }) => fonts.textStyle7.family};
    font-size: ${({ theme: { fonts } }) => fonts.textStyle7.size};
    font-weight: ${({ theme: { fonts } }) => fonts.textStyle7.weight};
    color: ${({ theme: { colors } }) => colors.warmGrey};
  }

  @media (max-width: 600px) {
    height: -webkit-fill-available;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
    padding-top: 1rem;

    p {
      width: 100%;
      margin-bottom: 1rem;
    }
  }
`;

export const PlusContainer = styled.div`
  cursor: pointer;

  grid-area: more;

  margin-left: 0.5rem;
  display: flex;
  flex-direction: row-reverse;
  align-items: center;
  justify-content: space-between;

  align-self: flex-start;

  svg {
    margin-right: 0.5rem;
  }

  h6 {
    font-family: ${({ theme: { fonts } }) => fonts.textStyle12.family};
    font-size: ${({ theme: { fonts } }) => fonts.textStyle12.size};
    font-weight: ${({ theme: { fonts } }) => fonts.textStyle12.weight};
    color: ${({ theme: { colors } }) => colors.mediumPink};
  }

  ${({ showInfo }) =>
    showInfo &&
    css`
      flex-direction: row;

      svg {
        margin-left: 0.5rem;
      }
    `}
  @media (max-width: 600px) {
    position: initial;
    width: 40vw;
    margin: 0 auto;

    ${({ showInfo }) =>
      showInfo &&
      css`
        justify-self: center;
        width: auto;
      `}
  }
`;

export const Description = styled.p`
  grid-column-start: descriptionBegin;
  grid-column-end: descriptionEnd;

  padding: 1rem 0;

  font-family: ${({ theme: { fonts } }) => fonts.textStyle7.family};
  font-size: ${({ theme: { fonts } }) => fonts.textStyle7.size};
  font-weight: ${({ theme: { fonts } }) => fonts.textStyle7.weight};
  color: ${({ theme: { colors } }) => colors.warmGrey};

  @media (max-width: 600px) {
    grid-area: description;
  }
`;
