import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import Card from "..";
import theme from "../../../styles/theme";

const makeSut = (props) => {
  return render(
    <ThemeProvider theme={theme}>
      <Card {...props} />
    </ThemeProvider>
  );
};

it("should render correct prop values", () => {
  const card = {
    title: "TEST-TITLE",
    subtitle: "TEST-SUBTITLE",
    smallSubtitle: "TEST-SMALL-SUBTITLE",
    photo: "",
    description: "TEST-DESCRIPTION",
  };
  makeSut(card);

  Object.values((text) => expect(screen.getByText(text)).toBeDefined());
});
