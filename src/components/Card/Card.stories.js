import React from "react";
import { ThemeProvider } from "styled-components";
import Card from ".";
import theme from "../../styles/theme";

export const Sample = (args) => (
  <ThemeProvider theme={theme}>
    <Card {...args} />
  </ThemeProvider>
);

export const MultipleSample = (args) => (
  <ThemeProvider theme={theme}>
    <Card {...args} />
    <Card {...args} />
    <Card {...args} />
    <Card {...args} />
  </ThemeProvider>
);

const story = {
  title: "Card",
  component: Sample,
  argTypes: {
    title: {
      control: "text",
      type: { name: "object", required: false },
      defaultValue: "Title",
      description:
        "_O valor da propriedade 'title' que é renderizado dentro do card_",
      table: {
        defaultValue: {
          summary: "Title",
          detail: "By default its set to 'any'",
        },
      },
    },
    subtitle: {
      control: "text",
      type: { name: "object", required: false },
      defaultValue: "Subtitle",
      description:
        "_O valor da propriedade 'subtitle' que é renderizado dentro do card_",
      table: {
        defaultValue: {
          summary: "Subtitle",
          detail: "By default its set to 'any'",
        },
      },
    },
    smallSubtitle: {
      control: "text",
      type: { name: "object", required: false },
      defaultValue: "Other",
      description:
        "_O valor da propriedade 'smallSubtitle' que é renderizado dentro do card_",
      table: {
        defaultValue: {
          summary: "Other",
          detail: "By default its set to 'any'",
        },
      },
    },
    description: {
      control: "text",
      type: { name: "object", required: false },
      defaultValue:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque accumsan quis sem a molestie. Curabitur ac nisi at quam consectetur aliquet.",
      description:
        "_O valor da propriedade 'description' que é renderizado dentro do card_",
      table: {
        defaultValue: {
          summary: "Lorem ipsum...",
          detail: "By default its set to 'Lorem ipsum...'",
        },
      },
    },
  },
};

const multipleStory = story;
multipleStory.title = "Cards";
multipleStory.component = MultipleSample;

export default story;
