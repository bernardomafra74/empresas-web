import styled, { keyframes } from "styled-components";

const rotation = keyframes`
   from {
    -webkit-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
`;

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  position: absolute;

  backdrop-filter: blur(1px);
  -webkit-backdrop-filter: blur(1px);

  z-index: 5000;

  > div {
    top: auto;
    bottom: auto;
  }

  div.circle {
    animation: ${rotation} 2s linear infinite;
    margin: 40vh auto;
    width: 8.25rem;
    height: 8.25rem;
    border-top: 10px solid ${({ theme: { colors } }) => colors.blue};
    border-right: 10px solid transparent;
    border-bottom: 10px solid ${({ theme: { colors } }) => colors.blue};
    border-left: 10px solid ${({ theme: { colors } }) => colors.blue};
    border-radius: 50%;
  }
`;
