import React from "react";
import * as Styles from "./styles";

const Loading = () => {
  return (
    <Styles.Container>
      <div>
        <div className="circle" />
      </div>
    </Styles.Container>
  );
};

export default Loading;
