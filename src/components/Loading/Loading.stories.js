import { action } from "@storybook/addon-actions";
import React from "react";
import { ThemeProvider } from "styled-components";
import Loading from ".";
import theme from "../../styles/theme";

export const Sample = (args) => (
  <ThemeProvider theme={theme}>
    <div onClick={action("start-loading")}>
      <Loading {...args} />
    </div>
  </ThemeProvider>
);

const story = {
  title: "Loading",
  component: Sample,
  argTypes: {},
};

export default story;
