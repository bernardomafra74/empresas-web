import React from "react";
import { ThemeProvider } from "styled-components";
import Input from ".";
import MailIcon from "../../assets/ic-email.svg";
import theme from "../../styles/theme";

export const Sample = (args) => (
  <ThemeProvider theme={theme}>
    <Input {...args} />
  </ThemeProvider>
);

// error,

const story = {
  title: "Input",
  component: Sample,
  parameters: { actions: { argTypesRegex: "^on.*" } },
  argTypes: {
    name: {
      type: { name: "string", required: false },
      defaultValue: "any",
      description: "_Valor da propriedade name do input_",
      table: {
        defaultValue: {
          summary: "any",
          detail: "By default its set to 'any'",
        },
      },
    },
    type: {
      defaultValue: "text",
      type: { name: "string", required: false },
      description:
        "_Valor da propriedade 'type' do input_ PS.: Atualmente só é suportado o tipo 'text'",
      table: {
        defaultValue: {
          summary: "text",
          detail: "By default its set to 'text'",
        },
      },
    },
    icon: {
      defaultValue: MailIcon,
      type: { name: "string", required: false },
      description:
        "_Ícone à esquerda do input para dar contexto e induzir uma melhor acessibilidade_  OBS.: A propriedade é um valor para o src da tag 'img'",
      table: {
        defaultValue: {
          summary: "ic-email.svg",
          detail: "By default its set to 'MailIcon'",
        },
      },
    },
    placeholder: {
      control: "text",
      type: { name: "string", required: false },
      defaultValue: "Digite um email válido...",
      description: "_Valor da propriedade placeholder do input_",
      table: {
        defaultValue: {
          summary: "Digite um email válido...",
          detail: "By default its set to 'Digite um email válido...'",
        },
      },
    },
    value: {
      control: "text",
      type: { name: "string", required: false },
      defaultValue: "",
      description: "_Texto ( Valor ) do input_",
      table: {
        defaultValue: {
          summary: "",
          detail: "By default its set to ''",
        },
      },
    },
    error: {
      type: { name: "string", required: false },
      control: "text",
      defaultValue: "",
      description: "_Texto de mensagem para erros do input_",
      table: {
        defaultValue: {
          summary: "",
          detail: "By default its set to ''",
        },
      },
    },
  },
};

export default story;
