import PropTypes from "prop-types";
import React from "react";
import { ReactComponent as NotVisiblePass } from "../../assets/not-visible.svg";
import { ReactComponent as Visible } from "../../assets/visible.svg";
import { ReactComponent as ErrorIcon } from "../../assets/warning.svg";
import * as Styles from "./styles";

const Input = ({
  name,
  type,
  icon,
  placeholder,
  onChange,
  onBlur,
  value,
  error,
}) => {
  const [visible, setVisible] = React.useState(false);

  const PasswordIcon = visible ? Visible : NotVisiblePass;
  const inputType = type === "password" && visible ? "text" : type;

  return (
    <Styles.Container className="input-container" error={error}>
      <Styles.InputWrapper
        hasIconOnRight={type === "password"}
        name={name}
        type={type}
        error={error}
      >
        {icon && <img src={icon} alt={name} />}
        <input
          name={name}
          type={inputType}
          placeholder={placeholder}
          onChange={onChange}
          value={value}
          error={error}
          onBlur={onBlur}
        />
        {type === "password" && !error ? (
          <PasswordIcon
            className="password"
            data-testid="password-ico"
            onClick={() => setVisible(!visible)}
          />
        ) : (
          error && <ErrorIcon className="error" data-testid="error-ico" />
        )}
      </Styles.InputWrapper>
      <span className="line" />
      {error && <Styles.ErrorMessage>{error}</Styles.ErrorMessage>}
    </Styles.Container>
  );
};

Input.propTypes = {
  type: PropTypes.string,
  icon: PropTypes.node,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string,
  error: PropTypes.string,
};

Input.defaultProps = {
  type: "text",
  icon: "",
  placeholder: "",
  name: "",
  onChange: () => {},
  onBlur: () => {},
  value: "",
  error: "",
};

export default Input;
