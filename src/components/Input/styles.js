import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-bottom: 2.112rem;

  span.line {
    width: 21.938rem;
    height: 0.105rem;

    margin: 0.22rem 0rem;

    background-color: ${({ error, theme: { colors } }) =>
      error ? colors.mediumPink : colors.charcoalGrey};
  }
`;

export const InputWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  height: calc(1.5rem + 0.22rem);

  position: relative;

  input {
    background-color: transparent;

    height: 1.5rem;
    border: none;
    width: ${({ hasIconOnRight, error }) =>
      hasIconOnRight || error ? "18rem" : "20rem"};

    padding-left: 0.709rem;

    ::placeholder {
      font-family: ${({ theme: { fonts } }) => fonts.textStyle2.family};
      font-size: ${({ theme: { fonts } }) => fonts.textStyle2.size};
      font-weight: ${({ theme: { fonts } }) => fonts.textStyle2.weight};
      color: ${({ theme: { colors } }) => colors.charcoalGrey};
      opacity: 0.5;
    }

    font-family: ${({ theme: { fonts } }) => fonts.textStyle8.family};
    font-size: ${({ theme: { fonts } }) => fonts.textStyle8.size};
    font-weight: ${({ theme: { fonts } }) => fonts.textStyle8.weight};
    color: ${({ theme: { colors } }) => colors.charcoalGreyTwo};

    :focus {
      outline: none;
    }
  }

  svg.password {
    cursor: pointer;
  }

  svg.error {
    width: 30px;
    height: 20px;
  }
`;

export const ErrorMessage = styled.span`
  /* position: absolute; */

  font-family: ${({ theme: { fonts } }) => fonts.textStyle12.family};
  font-size: ${({ theme: { fonts } }) => fonts.textStyle12.size};
  font-weight: ${({ theme: { fonts } }) => fonts.textStyle12.weight};
  color: ${({ theme: { colors } }) => colors.mediumPink};
`;
