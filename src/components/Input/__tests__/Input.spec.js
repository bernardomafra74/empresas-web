import { render } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import Input from "..";
import Mail from "../../../assets/ic-email.svg";
import theme from "../../../styles/theme";

const makeSut = (props) => {
  return render(
    <ThemeProvider theme={theme}>
      <Input {...props} />
    </ThemeProvider>
  );
};

it("should render type text", () => {
  const textComponent = makeSut({ type: "text" });
  const textInput = textComponent.getByRole("textbox");
  expect(textInput).toHaveAttribute("type", "text");
});

it("should render type password", () => {
  const passwordComponent = makeSut({ type: "password", placeholder: "Senha" });
  const passwordInput = passwordComponent.getByPlaceholderText("Senha");
  expect(passwordInput).toHaveAttribute("type", "password");
});
it("should render password-view icon if input type is password", () => {
  const passwordComponent = makeSut({
    type: "password",
    placeholder: "Senha",
    name: "password",
  });
  const passwordIcon = passwordComponent.getByTestId("password-ico");
  expect(passwordIcon).toBeDefined();
  expect(passwordIcon).toContainHTML(
    `<svg class="password" data-testid="password-ico">not-visible.svg</svg>`
  );
});

it("should render icon if input has valid icon props", () => {
  const passwordComponent = makeSut({
    type: "text",
    placeholder: "Email",
    icon: Mail,
    name: "email",
  });
  const mailIcon = passwordComponent.getByAltText("email");
  expect(mailIcon).toBeInTheDocument();
});

it("should render error icon if has valid error props", () => {
  const passwordComponent = makeSut({
    type: "password",
    placeholder: "Senha",
    error: "any",
  });
  const warningIcon = passwordComponent.getByTestId("error-ico");
  expect(warningIcon).toContainHTML(
    `<svg class="error" data-testid="error-ico">warning.svg</svg>`
  );
});
