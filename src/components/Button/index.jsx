import PropTypes from "prop-types";
import React from "react";
import * as Styles from "./styles";

const Button = ({ type, children, onClick }) => {
  return (
    <Styles.Button type={type} onClick={onClick}>
      {children}
    </Styles.Button>
  );
};

Button.propTypes = {
  type: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  type: "button",
  children: "",
  onClick: () => {},
};

export default Button;
