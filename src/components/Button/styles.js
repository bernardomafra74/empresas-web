import styled from "styled-components";

export const Button = styled.button`
  width: 21.75rem;
  height: 3.563rem;

  border: none;
  border-radius: 0.244rem;

  background-color: ${({ theme: { colors } }) => colors.blue};
  color: ${({ theme: { colors } }) => colors.whiteTwo};

  cursor: pointer;

  font-family: ${({ theme: { fonts } }) => fonts.textStyle5.family};
  font-size: ${({ theme: { fonts } }) => fonts.textStyle5.size};
  font-weight: ${({ theme: { fonts } }) => fonts.textStyle5.weight};

  :focus {
    outline: none;
  }

  :hover {
    opacity: 0.75;
  }
`;
