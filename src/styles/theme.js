const theme = {
  colors: {
    white: "#d8d8d8",
    whiteTwo: "#ffffff",
    darkIndigo: "#1a0e49",
    warmGrey: "#8d8c8c",
    charcoalGreen: "#403e4d",
    greyish: "#b5b4b4",
    charcoalGrey: "#383743",
    charcoalGreyTwo: "#403e4d",
    black54: "#808080",
    nightBlue: "#0d0430",
    mediumPink: "#ee4c77",
    darkRed: "#991237",
    blue: "#57bbbc",
    background: "#eeecdb",
    disabled: "#748383",
    homeBg: "#ebe9d7",
  },
  fonts: {
    textStyle1: {
      family: "'Roboto', 'sans-serif'",
      size: "1.125rem",
      weigth: "normal",
    },
    textStyle2: {
      family: "'Roboto', 'sans-serif'",
      size: "1.125rem",
      weigth: "normal",
    },
    textStyle3: {
      family: "'Roboto', 'sans-serif'",
      size: "2rem",
      weigth: "normal",
    },
    textStyle4: {
      family: "'Roboto', 'sans-serif'",
      size: "2.125rem",
      weigth: "normal",
    },
    textStyle5: {
      family: "'Lato', 'sans-serif'",
      size: "1.208rem",
      weigth: "normal",
    },
    textStyle6: {
      family: "'Roboto', 'sans-serif'",
      size: "1.3rem",
      weigth: "bold",
    },
    textStyle7: {
      family: "'Roboto', 'sans-serif'",
      size: "1.1rem",
      weigth: "normal",
    },
    textStyle8: {
      family: "'Roboto', 'sans-serif'",
      size: "1.369rem",
      weigth: "normal",
    },
    textStyle9: {
      family: "'Source Sans Pro', 'sans-serif'",
      size: "1.5rem",
      weigth: "normal",
    },
    textStyle10: {
      family: "'Roboto', 'sans-serif'",
      size: "1.5rem",
      weigth: "bold",
    },
    textStyle11: {
      family: "'Roboto', 'sans-serif'",
      size: "1.6rem",
      weigth: "bold",
    },
    textStyle12: {
      family: "'Roboto', 'sans-serif'",
      size: "0.8rem",
      weigth: "normal",
    },
  },
};

export default theme;
